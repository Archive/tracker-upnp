/*
 * Copyright © 2011 Intel Corporation.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

using GUPnP;

public errordomain UPnPSparqlError {
	NO_SUCH_TRIPLE,
}

namespace Tracker.UPnP.UPnPSparql {

public static void set_all_content_directories_unavailable (Sparql.Connection connection,
							    GLib.TimeVal timeval) throws Error {

	Sparql.Builder builder = new Sparql.Builder.update ();

	/* Mark all upnp datasources unavailable */
	builder.delete_open (null);
	builder.subject_variable ("urn");
	builder.predicate ("upnp:available");
	builder.object_boolean (true);
	builder.delete_close ();
	builder.where_open ();
	builder.subject_variable ("urn");
	builder.predicate ("a");
	builder.object ("upnp:ContentDirectory");
	builder.append ("FILTER (upnp:lastAvailableDate(?urn) < \"" + timeval.to_iso8601 () + "\") .");
	builder.where_close ();

	/* mark all upnp objects unavailable */
	builder.delete_open (null);
	builder.subject_variable ("urn");
	builder.predicate ("tracker:available");
	builder.object_boolean (true);
	builder.delete_close ();
	builder.where_open ();
	builder.subject_variable ("urn");
	builder.predicate ("a");
	builder.object ("upnp:UPnPDataObject");
	builder.append ("FILTER (nie:lastRefreshed(?urn) < \"" + timeval.to_iso8601 () + "\") .");
	builder.where_close ();

	connection.update (builder.result);
}

public class ContentDirectory : Object {
	static const string DIDL_IRI = "<urn:upnp-resource:%s>";

	Sparql.Connection _connection;
	string _urn;
	string _name;

	public ContentDirectory (Sparql.Connection connection, string service_urn, string service_name) {
		_connection = connection;
		_urn = service_urn;
		_name = service_name;
	}

	public void set_unavailable () throws Error {
		Sparql.Builder builder = new Sparql.Builder.update ();
		time_t timestamp = time_t ();

		/* Mark datasource unavailable, update timestamp */
		builder.delete_open (null);
		builder.subject_iri (_urn);
		builder.predicate ("upnp:available");
		builder.object_boolean (true);
		builder.predicate ("upnp:lastAvailableDate");
		builder.object_variable ("date");
		builder.delete_close ();
		builder.where_open ();
		builder.subject_iri (_urn);
		builder.predicate ("a");
		builder.object ("upnp:ContentDirectory");
		builder.append (" OPTIONAL { ");
		builder.subject_iri (_urn);
		builder.predicate ("upnp:lastAvailableDate");
		builder.object_variable ("date");
		builder.append (" } .\n");
		builder.where_close ();

		builder.insert_open (null);
		builder.subject_iri (_urn);
		builder.predicate ("upnp:lastAvailableDate");
		builder.object_date (ref timestamp);
		builder.insert_close ();

		/* mark all objects in datasource unavailable */
		builder.delete_open (_urn);
		builder.subject_variable ("urn");
		builder.predicate ("tracker:available");
		builder.object_boolean (true);
		builder.delete_close ();
		builder.where_open ();
		builder.subject_variable ("urn");
		builder.predicate ("a");
		builder.object ("upnp:UPnPDataObject");
		builder.where_close ();

		_connection.update (builder.result);
	}

	public void set_available () throws Error {
		time_t timestamp = time_t ();
		Sparql.Builder builder = new Sparql.Builder.update ();

		/* insert (or update) datasource for the contentdirectory */
		builder.delete_open (null);
		builder.subject_iri (_urn);
		builder.predicate ("nie:title");
		builder.object_variable ("title");
		builder.predicate ("upnp:lastAvailableDate");
		builder.object_variable ("date");
		builder.delete_close ();
		builder.where_open ();
		builder.subject_iri (_urn);
		builder.predicate ("a");
		builder.object ("upnp:ContentDirectory");
		builder.append (" OPTIONAL { ");
		builder.subject_iri (_urn);
		builder.predicate ("nie:title");
		builder.object_variable ("title");
		builder.append (" } .\n");
		builder.append (" OPTIONAL { ");
		builder.subject_iri (_urn);
		builder.predicate ("upnp:lastAvailableDate");
		builder.object_variable ("date");
		builder.append (" } .\n");
		builder.where_close ();

		builder.insert_open (null);
		builder.subject_iri (_urn);
		builder.predicate ("a");
		builder.object ("upnp:ContentDirectory");
		builder.predicate ("nie:title");
		builder.object_unvalidated (_name);
		builder.predicate ("upnp:lastAvailableDate");
		builder.object_date (ref timestamp);
		builder.predicate ("upnp:available");
		builder.object_boolean (true);
		builder.insert_close ();

		/* mark objects in datasource available */
		builder.insert_open (_urn);
		builder.subject_variable ("urn");
		builder.predicate ("tracker:available");
		builder.object_boolean (true);
		builder.insert_close ();
		builder.where_open ();
		builder.subject_variable ("urn");
		builder.predicate ("a");
		builder.object ("upnp:UPnPDataObject");
		builder.where_close ();

		_connection.update (builder.result);
	}

	public void set_update_id (uint id, time_t refresh_time) throws Error {
		Sparql.Builder builder = new Sparql.Builder.update ();
		var tm = GLib.Time.gm (refresh_time);
		string filter = "FILTER (?reftime < \"%04d-%02d-%02dT%02d:%02d:%02dZ\")".printf
				(tm.year + 1900, tm.month + 1, tm.day, tm.hour, tm.minute, tm.second);

		builder.delete_open (null);
		builder.subject_iri (_urn);
		builder.predicate ("upnp:systemUpdateID");
		builder.object_variable ("id");
		builder.predicate ("upnp:lastAvailableDate");
		builder.object_variable ("date");
		builder.delete_close ();
		builder.where_open ();
		builder.subject_iri (_urn);
		builder.predicate ("a");
		builder.object ("upnp:ContentDirectory");
		builder.append (" OPTIONAL { ");
		builder.subject_iri (_urn);
		builder.predicate ("upnp:systemUpdateID");
		builder.object_variable ("id");
		builder.append (" } .\n");
		builder.append (" OPTIONAL { ");
		builder.subject_iri (_urn);
		builder.predicate ("upnp:lastAvailableDate");
		builder.object_variable ("date");
		builder.append (" } .\n");
		builder.where_close ();

		builder.insert_open (null);
		builder.subject_iri (_urn);
		builder.predicate ("upnp:systemUpdateID");
		builder.object_int64 (id);
		builder.predicate ("upnp:lastAvailableDate");
		builder.object_date (ref refresh_time);
		builder.insert_close ();

		/* delete objects that were not updated on refresh_time */
		builder.delete_open (_urn);
		builder.subject_variable ("urn");
		builder.predicate ("a");
		builder.object ("rdfs:Resource");
		builder.delete_close ();
		builder.where_open ();
		builder.subject_variable ("urn");
		builder.predicate ("nie:lastRefreshed");
		builder.object_variable ("reftime");
		builder.append (filter);
		builder.where_close ();

		_connection.update (builder.result);
	}

	public uint get_update_id () throws Error {
		string query = "SELECT ?id WHERE { <%s> upnp:systemUpdateID ?id . }".printf (_urn);
		Sparql.Cursor cursor = _connection.query (query, null);

		if (!cursor.next (null)) {
			throw new UPnPSparqlError.NO_SUCH_TRIPLE ("Failed to get systemUpdateID");
		}

		return (uint)cursor.get_integer (0);
	}

	public void set_media_item (DIDLLiteItem item, time_t refresh_time) throws Error {
		Sparql.Builder builder = new Sparql.Builder.update ();
		string nfo_type, nmm_type, item_iri;
		string? artist, album, thumb;
		GLib.List<DIDLLiteResource>? resources;

		if (item.upnp_class.has_prefix ("object.item.imageItem")) {
			nmm_type = "nmm:Photo";
			nfo_type = "nfo:Image";
		} else if (item.upnp_class.has_prefix ("object.item.audioItem")) {
			nmm_type = "nmm:MusicPiece";
			nfo_type = "nfo:Audio";
		} else if (item.upnp_class.has_prefix ("object.item.videoItem")) {
			nmm_type = "nmm:Video";
			nfo_type = "nfo:Video";
			/* "nfo:Audio" will be added below */
		} else {
			warning ("Unrecognised upnp_class '%s'", item.upnp_class);
			/* throw ? */
			return;
		}

		/* TODO: should handle all/more resources, not just the first?
		   The ontology allows for this (see nmm:alternativeMedia), 
		   but I'm not sure if the clients do...  */
		resources = item.get_resources (); 
		if (resources == null) {
			warning ("item has no resources, skipping");
			return;
		}

		/* create artist, album */
		artist = item.artist;
		handle_artist (builder, artist);
		album = item.album;
		handle_album (builder, album);

		item_iri = Sparql.escape_uri_printf (DIDL_IRI, item.id);

		/* delete the media item if it exists */
		builder.delete_open (_urn);
		builder.subject (item_iri);
		builder.predicate ("a");
		builder.object ("rdfs:Resource");
		builder.delete_close ();

		/* create the media item */
		builder.insert_open (_urn);
		builder.subject (item_iri);

		builder.predicate ("a");
		builder.object ("upnp:UPnPDataObject");
		builder.object (nmm_type);
		builder.object (nfo_type);
		if (item.upnp_class.has_prefix ("object.item.videoItem")) {
			builder.object ("nfo:Audio");
		}

		builder.predicate ("nie:dataSource");
		builder.object_unvalidated (_urn);

		if (item.title != null) {
			builder.predicate ("nie:title");
			builder.object_unvalidated (item.title);
		}

		/* NOTE: should use item.get_artists (), but
		 * it's currently broken (gupnp-av 0.7.1) */
		if (item.upnp_class.has_prefix ("object.item.audioItem")) {
			if (artist != null) {
				string urn = Sparql.escape_uri_printf ("urn:artist:%s",
								       artist);
				builder.predicate ("nmm:performer");
				builder.object_string (urn);
			}

			if (album != null) {
				string urn = Sparql.escape_uri_printf ("urn:album:%s",
								       album);
				builder.predicate ("nmm:musicAlbum");
				builder.object_string (urn);

				if (item.track_number > -1) {
					builder.predicate ("nmm:trackNumber");
					builder.object_int64 (item.track_number);
				}
			}
		}

		thumb = item.album_art;
		if (thumb == null) {
			thumb = get_thumbnail_from_resources (resources);
		}

		if (thumb != null) {
			builder.predicate ("upnp:thumbnail");
			builder.object_unvalidated (thumb);
		}

		/* prevent a local media server from serving this item */
		builder.predicate ("nmm:uPnPShared");
		builder.object_boolean (false);

		builder.predicate ("nie:lastRefreshed");
		builder.object_date (ref refresh_time);

		builder.predicate ("tracker:available");
		builder.object_boolean (true);

		/* get predicate-object pairs from resource */
		handle_resource (builder, resources.data);

		builder.insert_close ();

		_connection.update (builder.result);
	}

	private string? get_thumbnail_from_resources (GLib.List<DIDLLiteResource> resources) {
		DIDLLiteResource? jpeg_tn_res, jpeg_sm_res;

		jpeg_tn_res = jpeg_sm_res = null;

		/* accept any JPEG_SM (small), or if that's not available, 
		 * JPEG_TN (thumbnail) */
		/* NOTE: should save all *_TN resource uris, this is just a 
		 * first attempt... */
		foreach (DIDLLiteResource res in resources) {
			ProtocolInfo? info;
			info = res.protocol_info;

			if (info.protocol.has_prefix ("http-get")) {
				string? profile = info.dlna_profile;

				if (profile == "JPEG_SM") {
					jpeg_sm_res = res;
					/* we're good now */
					break;
				} else if (profile == "JPEG_TN") {
					jpeg_tn_res = res;
				}
			}
		}

		if (jpeg_sm_res != null) {
			return jpeg_sm_res.uri;
		} else if (jpeg_tn_res != null) {
			return jpeg_tn_res.uri;
		}
		return null;
	}

	private void handle_artist (Tracker.Sparql.Builder builder,
	                            string? artist) {
		if (artist == null) {
			return;
		}

		string artist_urn = Sparql.escape_uri_printf ("urn:artist:%s",
		                                              artist);

		builder.insert_open (_urn);
		builder.subject_iri (artist_urn);

		builder.predicate ("a");
		builder.object ("nmm:Artist");

		builder.predicate ("nmm:artistName");
		builder.object_unvalidated (artist);

		builder.insert_close ();
	}

	private void handle_album (Tracker.Sparql.Builder builder,
	                           string? album) {
		if (album == null) {
			return;
		}

		string album_urn = Sparql.escape_uri_printf ("urn:album:%s", album);

		builder.insert_open (_urn);
		builder.subject_iri (album_urn);

		builder.predicate ("a");
		builder.object ("nmm:MusicAlbum");

		builder.predicate ("nmm:albumTitle");
		builder.object_unvalidated (album);

		builder.insert_close ();
	}

	private void handle_resource (Tracker.Sparql.Builder builder,
	                              DIDLLiteResource resource) {
		int width, height, bits_per_sample,
		    audio_channels, color_depth;
		long duration;
		int64 size;
		ProtocolInfo? info;

		info = resource.protocol_info;
		if (info != null) {
			if (info.dlna_profile != null) {
				builder.predicate ("nmm:dlnaProfile");
				builder.object_unvalidated (info.dlna_profile);
			}
			if (info.mime_type != null) {
				builder.predicate ("nie:mimeType");
				builder.object_unvalidated (resource.protocol_info.mime_type);
			}
		}

		builder.predicate ("nie:url");
		builder.object_unvalidated (resource.uri);

		height = resource.height;
		if (height > 0) {
			builder.predicate ("nfo:height");
			builder.object_int64 (height);
		}

		width = resource.width;
		if (width > 0) {
			builder.predicate ("nfo:width");
			builder.object_int64 (width);
		}

		size = resource.size64;
		if (size > 0) {
			builder.predicate ("nie:byteSize");
			builder.object_int64 (size);
		}

		duration = resource.duration;
		if (duration > 0) {
			builder.predicate ("nfo:duration");
			builder.object_int64 (duration);
		}

		bits_per_sample = resource.bits_per_sample;
		if (bits_per_sample > 0) {
			builder.predicate ("nfo:bitsPerSample");
			builder.object_int64 (bits_per_sample);
		}

		color_depth = resource.color_depth;
		if (color_depth > 0) {
			builder.predicate ("nfo:colorDepth");
			builder.object_int64 (color_depth);
		}

		audio_channels = resource.audio_channels;
		if (audio_channels > 0) {
			builder.predicate ("nfo:channels");
			builder.object_int64 (audio_channels);
		}
	}
}

}
