/*
 * Copyright © 2011 Intel Corporation.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/*
 * TODO: make sure we don't mine local mediaservers?
 */


using GUPnP;

namespace Tracker.UPnP {

public class Miner : Tracker.Miner, GLib.Initable {

	private static const string MINER_NAME = "UPnP";
	private static const string MINER_DESCRIPTION = "Tracker miner for UPnP ContentDirectories";
	private static const string MEDIASERVER_DEVICE = "urn:schemas-upnp-org:device:MediaServer:1";

	private static MainLoop main_loop;
	private int exit_code;

	private ContextManager manager;

	private HashTable<string, ContentDirectory?> mediaservers;

	private TimeVal init_time;
	private uint init_timeout_id;
	private bool _running = true;
	private bool running {
		get {
			return _running; 
		}
		set {
			_running = value;
			update_paused ();
		}
	}

	construct {
		name = MINER_NAME;
	}

	public bool init (GLib.Cancellable? cancellable) throws GLib.Error {
		base.init (cancellable);

		main_loop = new MainLoop (null, false);
		manager = new ContextManager (null, 0);
		mediaservers = new HashTable<string, ContentDirectory?> (str_hash, str_equal);
		_running = true;
		init_time = TimeVal ();

		exit_code = 0;
		update_status ();
		update_progress ();

		return true;
	}

	public override void started () {
		running = true;
	}

	public override void paused () {
		running = false;
	}

	public override void resumed () {
		running = true;
	}

	public override void stopped () {
		running = false;
	}

	private void update_paused () {
		HashTableIter<string,ContentDirectory> iter = HashTableIter<string,ContentDirectory> (mediaservers);
		ContentDirectory? dir;

		while (iter.next (null, out dir)) {
			if (dir != null) {
				dir.paused = !running;
			}
		}
	}

	private void update_status () {
		HashTableIter<string,ContentDirectory> iter = HashTableIter<string,ContentDirectory> (mediaservers);
		ContentDirectory? dir;
		ContentDirectory.State state = ContentDirectory.State.IDLE;
		string server_string = "";
		uint count;

		while (iter.next (null, out dir)) {
			if (dir == null) {
				return;
			}

			server_string = dir.name;

			if (dir.state == ContentDirectory.State.INDEXING) {
				state = ContentDirectory.State.INDEXING;
				break;
			}
		}

		count = mediaservers.size();
		switch (count) {
		case 0:
			server_string = "no mediaservers";
			break;
		case 1:
			/* name is set already */
			break;
		default:
			server_string = "%u mediaservers".printf (count);
			break;
		}

		switch (state) {
			case ContentDirectory.State.INDEXING:
				status = "Indexing %s".printf (server_string);
				break;
			default:
				status = "Idle (%s)".printf (server_string);
				break;
		}
	}

	private void update_progress () {
		HashTableIter<string,ContentDirectory> iter = HashTableIter<string,ContentDirectory> (mediaservers);
		ContentDirectory? dir;
		double total_progress = 0.0;
		int count = 0;

		while (iter.next (null, out dir)) {
			if (dir != null) {
				total_progress += dir.progress;
			}

			count++;
		}

		if (count == 0) {
			progress = 1.0;
		} else {
			progress = total_progress / count;
		}
	}

	private void on_dir_state_notify (ParamSpec spec) {
		update_status ();
	}

	private void on_dir_progress_notify (ParamSpec spec) {
		update_progress ();
	}

	private void on_device_unavailable (ControlPoint cp,
	                                    DeviceProxy device) {
		mediaservers.remove (device.udn);
		update_status ();
		update_progress ();
	}

	private void on_device_available (ControlPoint cp,
	                                  DeviceProxy  device) {
		ContentDirectory dir;
		Sparql.Connection? conn = get_connection ();

		if (mediaservers.lookup (device.udn) != null) {
			return;
		}

		if (conn == null) {
			warning ("Cannot handle new mediaserver without a Sparql connection");
			return;
		}

		try {
			dir = new ContentDirectory (device, (!)conn, !running);
			dir.notify["progress"].connect (on_dir_progress_notify);
			dir.notify["state"].connect (on_dir_state_notify);
			mediaservers.insert (device.udn, dir);
		} catch (ContentDirectoryError error){
			warning ("Failed to create new UPnPDir: %s",
			         error.message);
		}
	}

	private void on_context_available (Context context) {
		if (init_timeout_id != 0) {
		    GLib.Source.remove (init_timeout_id);
		    init_timeout_id = 0;
		}
		ControlPoint cp = new ControlPoint (context, MEDIASERVER_DEVICE);

		cp.device_proxy_available.connect (on_device_available);
		cp.device_proxy_unavailable.connect (on_device_unavailable);

		manager.manage_control_point (cp);
		cp.active = true;
	}

	private int run () {
		manager.context_available.connect (on_context_available);

		/* mark everything unavailable if it doesn't appear in a few 
		 * seconds */
		init_time.get_current_time ();
		init_timeout_id = GLib.Timeout.add_seconds (5, () =>
			{
				try
				{
					warning ("Connection attempt timed out");
					UPnPSparql.set_all_content_directories_unavailable (get_connection(),
											    init_time);
				}
				catch (GLib.Error err)
				{
					warning ("Failed to set all directories unavailable: %s",
						 err.message);
				}
				return false;
			});

		main_loop.run ();
		return exit_code;
	}

#if G_OS_WIN32
#else
	private static bool in_loop = false;
	private static void signal_handler (int signo) {
		if (in_loop) {
			Posix.exit (Posix.EXIT_FAILURE);
		}

		switch (signo) {
			case Posix.SIGINT:
			case Posix.SIGTERM:
				in_loop = true;
				debug ("Received signal %d , quitting", signo);
				main_loop.quit ();
				break;
		}

	}
#endif

	private static void init_signals () {
#if G_OS_WIN32
#else
		Posix.sigaction_t act = Posix.sigaction_t ();
		Posix.sigset_t    empty_mask = Posix.sigset_t ();
		Posix.sigemptyset (empty_mask);
		act.sa_handler = signal_handler;
		act.sa_mask    = empty_mask;
		act.sa_flags   = 0;

		Posix.sigaction (Posix.SIGTERM, act, null);
		Posix.sigaction (Posix.SIGINT, act, null);
#endif
	}

	private static int main (string[] args) {
		Environment.set_application_name ("UPnP tracker miner");

		try {
			UPnP.Miner miner = Initable.new (typeof (UPnP.Miner), null) as UPnP.Miner;
			init_signals ();
			return miner.run ();
		} catch (Error e) {
			printerr ("Couldn't create new UPnP Miner: '%s', exiting...\n",
			          e.message);
			return -1;
		}

	}
}

}
