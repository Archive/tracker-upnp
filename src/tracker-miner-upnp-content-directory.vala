/*
 * Copyright © 2011 Intel Corporation.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/* TODO use UPnP ContainerUpdateIDs to minimize network IO
 *      - check if containerupdateids are supported with introspection
 *      - should still subscribe to SystemUpdateID so it an be saved
 *        in tracker, and the initial search/crawl can be done on 
 *        startup if needed
 *      - after the initial search/crawl, use containerupdateids
 *        to browse only the changed content
 *   */


using GUPnP;

namespace Tracker.UPnP {

private errordomain ContentDirectoryError {
	NO_CONTENT_DIR,
	NO_CONNECTION,
}

public class ContentDirectory : Object
{
	private static const string CONTENT_DIR_SERVICE = "urn:schemas-upnp-org:service:ContentDirectory:1";
	private const uint ITEMS_PER_REQUEST = 100;
	private const uint MAX_CRAWL_DEPTH = 16;

	private class CrawlContainer {
		public string id;
		public uint index;
		public uint depth;
	}
	private enum IndexMode {
		SEARCH,
		CRAWL,
	}
	public enum State  {
		INITIALIZING,
		IDLE,
		INDEXING,
	}


	private ServiceProxy _service;
	private DIDLLiteParser _parser;
	private UPnPSparql.ContentDirectory _sparql_dir;

	private time_t _refresh_time;
	private IndexMode _mode;
	private bool _paused;

	private uint _update_id;
	private uint _indexed_id;
	private bool _force_index;

	private Queue<CrawlContainer?> _crawl_queue;

	public string name { get; private set; }
	public double progress { get; private set; default = 0.0; }
	public State state { get; private set; default = State.INITIALIZING; }

	public bool paused {
		get { return _paused; }
		set {
			_paused = value;
			if (!_paused) {
				start_indexing ();
			}
		}
	}

	public ContentDirectory (DeviceProxy device,
	                         Sparql.Connection connection,
	                         bool paused) throws ContentDirectoryError {
		ServiceProxy? s = (ServiceProxy?)device.get_service (CONTENT_DIR_SERVICE);
		if (s == null) {
			throw new ContentDirectoryError.NO_CONTENT_DIR ("Device has no ContentDirectory");
		}
		_service = (!)s;

		name = device.get_friendly_name ();
		_paused = paused;
		_parser = new DIDLLiteParser ();
		_parser.item_available.connect (on_item_available);
		_parser.container_available.connect (on_container_available);
		_crawl_queue = new Queue<CrawlContainer?> ();
		_sparql_dir = new UPnPSparql.ContentDirectory (connection,
		                                               "urn:nepomuk:datasource:%s".printf (_service.udn),
		                                               name);

		_service.begin_action ("GetSearchCapabilities",
		                       get_search_capabilities_cb,
		                       null);
	}

	~ContentDirectory () {
		debug ("Marking ContentDirectory media unavailable");
		try {
			_sparql_dir.set_unavailable ();
		} catch (Error err){
			warning ("Failed to mark media unavailable: %s", err.message);
		}
	}

	private void initialize () {
		try {
			_sparql_dir.set_available ();
		} catch (Error err) {
			warning ("Failed to insert the contentdirectory datasource: %s",
			         err.message);
		}

		get_tracker_update_id ();

		/* possibly not true -- could set this after a few seconds timeout? */
		state = State.IDLE;
		progress = 1.0;

		/* no need to start indexing, initial notify will start it */
		_service.subscribed = true;
		_service.add_notify ("SystemUpdateID",
		                     typeof (uint),
		                     on_system_update_id_changed);
	}

	private void start_indexing () {
		/* indexing will happen after init */
		if (state == State.INITIALIZING) {
			return;
		}

		/* no need to index if we're up to date. */
		if (!_force_index &&
		    state == State.IDLE && 
		    _update_id == _indexed_id) {
			progress = 1.0;
			debug ("systemUpdateIDs match, no need to index");
			return;
		}

		state = State.INDEXING;

		if (paused) {
			debug ("Need indexing, but we're paused...");
			return;
		}

		/* add root to crawl queue if we're starting from scratch */
		if (_update_id != _indexed_id || _force_index) {
			_crawl_queue.push_tail (new CrawlContainer () { id = "0", index = 0, depth = 0 });
			_refresh_time = time_t ();
			_indexed_id = _update_id;
		}

		_force_index = false;

		if (_mode == IndexMode.SEARCH) {
			search ();
		} else {
			crawl ();
		}
	}

	private void crawl () {
		CrawlContainer cont = _crawl_queue.peek_head ();
		assert (_crawl_queue.peek_head () != null);

		debug ("crawling container %s index %u (SystemUpdateID %u)",
		       cont.id, cont.index, _indexed_id);
		_service.begin_action ("Browse", index_cb,
		                       "ObjectID", typeof (string), cont.id,
		                       "BrowseFlag", typeof (string), "BrowseDirectChildren",
		                       "Filter", typeof (string), "*",
		                       "StartingIndex", typeof (uint), cont.index,
		                       "RequestedCount", typeof (uint), ITEMS_PER_REQUEST,
		                       "SortCriteria", typeof (string), "",
		                       null);
	}

	private void search () {
		CrawlContainer cont = _crawl_queue.peek_head ();
		assert (_crawl_queue.peek_head () != null);

		debug ("Searching at index %u (SystemUpdateID %u)",
		       cont.index, _indexed_id);
		_service.begin_action ("Search", index_cb,
		                       "ContainerID", typeof (string), cont.id,
		                       "SearchCriteria", typeof (string), "*",
		                       "Filter", typeof (string), "*",
		                       "StartingIndex", typeof (uint), cont.index,
		                       "RequestedCount", typeof (uint), ITEMS_PER_REQUEST,
		                       "SortCriteria", typeof (string), "",
		                       null);
	}

	private void get_tracker_update_id () {
		try {
			_indexed_id = _sparql_dir.get_update_id ();
			_update_id = _indexed_id;
			debug ("Saved systemUpdateID is %u", _indexed_id);
		} catch  (UPnPSparqlError error) {
			/* no systemUpdateID saved */
			_force_index = true;
		} catch  (Error error) {
			warning ("Failed to get systemUpdateID from tracker: %s", error.message);
		}
	}

	private void set_tracker_update_id () {
		try {
			_sparql_dir.set_update_id (_indexed_id, _refresh_time);
		} catch (Error err) {
			warning ("Failed to insert the contentdirectory datasource: %s",
			         err.message);
		}
	}

	private void on_system_update_id_changed (ServiceProxy proxy,
	                                          string variable,
	                                          Value val) {
		if (!val.holds (typeof (uint))) {
			warning ("SystemUpdateID is not a uint");
		} else {
			_update_id = val.get_uint ();
			debug ("Mediaserver SystemUpdateID is %u", _update_id);
		}

		progress = 0.0;
		if (state == State.IDLE) {
			start_indexing ();
		}
	}

	private void on_container_available (DIDLLiteParser parser,
	                                     DIDLLiteContainer didl_cont) {
		if (_mode == IndexMode.CRAWL) {
			CrawlContainer parent = _crawl_queue.peek_head ();
			assert (_crawl_queue.peek_head () != null);
			if (parent.depth < MAX_CRAWL_DEPTH) {
				CrawlContainer cont = new CrawlContainer ();
				cont.id = didl_cont.id;
				cont.index = 0;
				cont.depth = parent.depth + 1;
				_crawl_queue.push_tail (cont);
			} else {
				warning ("Not crawling container '%s': directory deeper than %u levels",
				         didl_cont.id, MAX_CRAWL_DEPTH);
			}
			return;
		}
	}

	private void on_item_available (DIDLLiteParser parser,
	                                DIDLLiteItem item)
	{
		try {
			_sparql_dir.set_media_item (item, _refresh_time);
		} catch (Error err){
			warning ("Failed to update item: %s", err.message);
		}
	}

	private void index_cb (ServiceProxy service, ServiceProxyAction action){
		string didl;
		uint returned = 0;
		uint total = 0;
		CrawlContainer cont = _crawl_queue.peek_head();

		if (_update_id != _indexed_id) {
			debug ("Canceling indexing, UpdateID has changed");
			_crawl_queue.clear ();
			start_indexing ();
			return;
		}

		try {
			service.end_action (action,
			                    "Result", typeof (string), out didl,
			                    "NumberReturned", typeof (uint), out returned,
			                    "TotalMatches", typeof (uint), out total,
			                    null);

			/* update tracker db */
			debug ("got %u indexing results out of %u total", returned, total);

			if (returned > 0) {
				try {
					_parser.parse_didl (didl);
				} catch (Error err) {
					warning ("Failed to parse DIDL fragment: %s",
					         err.message);
				}
				cont.index += returned;
			}

			/* total can be 0 (meaning server does not know) */
			/* in real world total is also sometimes larger than 
			   number of actually returned items */
			if ((total > 0 && cont.index >= total) ||
			    returned == 0) {

				if (total > 0 && cont.index < total) {
					debug ("Container '%s' claims to have %u items, only returned %u.",
					       cont.id, total, cont.index);
				}

				/* remove indexed container from queue */
				_crawl_queue.pop_head ();
			}

		} catch (Error err) {
			warning ("Search/Browse failed: %s", err.message);
			_crawl_queue.pop_head ();
		}


		if (_crawl_queue.peek_head () == null) {
			/* indexing finished */
			set_tracker_update_id ();
			state = State.IDLE;
			progress = 1.0;
		} else if (!paused) {
			/* continue indexing */
			if (_mode == IndexMode.SEARCH) {
				if (total > 0) {
					progress = cont.index / (double)total;
				} else {
					progress = 0.0;
				}
				search ();
			} else {
				crawl ();
			}
		}
	}

	private void get_search_capabilities_cb (ServiceProxy service,
	                                         ServiceProxyAction action) {
		string caps;

		try {
			service.end_action (action,
			                    "SearchCaps", typeof (string), out caps,
			                    null);
		} catch (Error err) {
			warning ("GetSearchCapabilities failed: %s", err.message);
			caps = "";
		}

		/* TODO: do comparison properly: split the caps with
		 * commas, strip whitespace, compare each cap */
		if ("*" in caps ||
		    ("upnp:class" in caps &&
		     "dc:title" in caps &&
		     "upnp:artist" in caps &&
		     "upnp:album" in caps)) {
			_mode = IndexMode.SEARCH;
		} else {
			_mode = IndexMode.CRAWL;
		}
		initialize ();
	}

}

}
